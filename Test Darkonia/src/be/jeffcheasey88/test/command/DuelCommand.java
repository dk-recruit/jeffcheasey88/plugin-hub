package be.jeffcheasey88.test.command;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import be.jeffcheasey88.test.Main;
import be.jeffcheasey88.test.utils.DuelManager.Arena;

public class DuelCommand implements CommandExecutor {
	
	private static HashMap<Player, Arena> selection = new HashMap<>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] a) {
		boolean isPlayer = (sender instanceof Player);
		boolean admin = ((isPlayer) ? ((Player)sender).isOp() : true);
		
		if(a.length == 0){
			if(!isPlayer){
				sender.sendMessage("/duel maintenance <on/off>");
				return false;
			}
			Player player = (Player)sender;
			if(Main.duelManager.isInDuel(player)){
				sender.sendMessage("Tu es d�j� en duel...");
				return false;
			}
			if(Main.queue_duel.contains(player)){
				sender.sendMessage("/duel leave - Permet de quitter un duel ou la queue des duels");
				return false;
			}
			Main.queue_duel.addPlayer(player, "", 0);
			player.sendMessage("Vous avez rejoind la queue des duels");
			return false;
		}
		
		if(a[0].equalsIgnoreCase("leave")){
			if(!isPlayer){
				sender.sendMessage("seul un joueur peut faire cela !");
				return false;
			}
			Player player = (Player)sender;
			if(Main.duelManager.isInDuel(player)){
				//a voir
				return false;
			}
			if(Main.queue_duel.contains(player)){
				Main.queue_duel.removePlayer(player);
				player.sendMessage("Vous avez quitter la queue duel !");
			}
		}
		
		if(admin){
			if(a[0].equalsIgnoreCase("maintenance")){
				if(a.length == 1){
					sender.sendMessage("/duel maintenance <on/off>");
					return false;
				}
				if(a[1].equalsIgnoreCase("on")){
					Main.queue_duel.stop();
					sender.sendMessage("Maintenance activ�e !");
				}else if(a[1].equalsIgnoreCase("off")){
					Main.queue_duel.start();
					sender.sendMessage("Maintenance d�sactiv�e !");
				}else{
					sender.sendMessage("/duel maintenance <on/off>");
				}
			}else if(!isPlayer){
				sender.sendMessage("seul un joueur peut faire cela !");
				return false;
			}
			
			if(a[0].equalsIgnoreCase("pnj")){
				//on m'a pas dit ce qu'il doit faire...
			}else if(a[0].equalsIgnoreCase("setpose")){
				if(a.length == 1){
					sender.sendMessage("/duel setpose <1|2> pour d�finir les points de spawn des arenes");
					return false;
				}
				int pos = 1;
				try {
					pos = Integer.parseInt(a[1]);
					if(pos < 1 || pos > 2){
						sender.sendMessage("/duel setpose <1|2> pour d�finir les points de spawn des arenes");
						return false;
					}
				}catch(Exception e){
					sender.sendMessage(a[1]+" n'est pas un nombre...");
					return false;
				}
				Player player = (Player)sender;
				if(selection.containsKey(player)){
					Arena arena = selection.get(player);
					arena.setSpawn(player.getLocation(), pos);
					if(pos == 2){
						int id = 0;
						while(!(Arena.isPathFree("arena"+id))){
							id+=1;
						}
						arena.setName("arena"+id);
						arena.save(Main.getInstance().getConfig(), "duel.arena"+id);
						selection.remove(player);
						sender.sendMessage("sauver sous le nom "+arena.getName());
					}else{
						selection.put(player, arena);
						sender.sendMessage("Premiere position d�finie !");
					}
				}else{
					Arena arena = new Arena();
					arena.setSpawn(player.getLocation(), 1);
					selection.put(player, arena);
					sender.sendMessage("Premiere position d�finie !");
				}
			}
		}
		
		return false;
	}

}
