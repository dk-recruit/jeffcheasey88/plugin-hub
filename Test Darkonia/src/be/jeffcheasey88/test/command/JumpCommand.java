package be.jeffcheasey88.test.command;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import be.jeffcheasey88.test.Main;
import be.jeffcheasey88.test.utils.Jump;
import be.jeffcheasey88.test.utils.TopUtils.TopInfo;

public class JumpCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] a) {
		boolean isPlayer = (sender instanceof Player);
		boolean admin = ((isPlayer) ? ((Player)sender).isOp() : true);
		
		if(a.length == 0){
			if(!isPlayer){
				sender.sendMessage("/jump maintenance <on/off>");
				return false;
			}
			Player player = (Player)sender;
			if(Jump.isInto(player)){
				sender.sendMessage("Tu es d�j� en jump...");
				sender.sendMessage("/jump leave - Permet de quitter le jump");
				return false;
			}
			Jump.spawn(player);
			player.sendMessage("Vous commencez le jump !");
			return false;
		}
		
		if(a[0].equalsIgnoreCase("leave")){
			if(!isPlayer){
				sender.sendMessage("seul un joueur peut faire cela !");
				return false;
			}
			Player player = (Player)sender;
			if(Jump.isInto(player)){
				Jump.quit(player);
				return false;
			}
		}else if(a[0].equalsIgnoreCase("top")){
			ArrayList<TopInfo> top = Main.jump_top.get();
			sender.sendMessage("-------- Top --------");
			for(int i = 0; i < top.size(); i++){
				TopInfo info = top.get(i);
				sender.sendMessage("["+(i+1)+"] "+info.getName()+" : "+info.getValue());
			}
			sender.sendMessage("---------------------");
		}
		
		if(admin){
			if(a[0].equalsIgnoreCase("maintenance")){
				if(a.length == 1){
					sender.sendMessage("/jump maintenance <on/off>");
					return false;
				}
				if(a[1].equalsIgnoreCase("on")){
					Main.queue_duel.stop();
					sender.sendMessage("Maintenance activ�e !");
				}else if(a[1].equalsIgnoreCase("off")){
					Main.queue_duel.start();
					sender.sendMessage("Maintenance d�sactiv�e !");
				}else{
					sender.sendMessage("/jump maintenance <on/off>");
				}
			}else if(!isPlayer){
				sender.sendMessage("seul un joueur peut faire cela !");
				return false;
			}
			if(a[0].equalsIgnoreCase("setholo") || a[0].equalsIgnoreCase("holomove")){
				Jump.moveHolo(((Player)sender).getLocation());
				sender.sendMessage("Les Holo ont bien �t� t�l�porter !");
			}else if(a[0].equalsIgnoreCase("removeholo")){
				Jump.removeHolo();
				sender.sendMessage("d�truit !");
			}else if(a[0].equalsIgnoreCase("setpoints")){
				
			}else if(a[0].equalsIgnoreCase("removepoints")){
				
			}else if(a[0].equalsIgnoreCase("listpoints")){
				
			}
		}
		return false;
	}

}
