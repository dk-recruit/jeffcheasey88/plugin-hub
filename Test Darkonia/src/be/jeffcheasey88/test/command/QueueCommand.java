package be.jeffcheasey88.test.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import be.jeffcheasey88.test.Main;

public class QueueCommand implements CommandExecutor {
	
	private String cmd;
	private boolean main_cmd;
	
	public QueueCommand(JavaPlugin plugin, String cmd, boolean main_cmd){
		this.cmd = cmd;
		this.main_cmd = main_cmd;
		plugin.getCommand(cmd).setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] a) {
		boolean isPlayer = (sender instanceof Player);
		boolean admin = ((isPlayer) ? ((Player)sender).isOp() : true);
		
		if(a.length == 0){
			if(!main_cmd){
				if(!isPlayer){
					sender.sendMessage("seul un joueur peut faire cela !");
					return false;
				}
				//look si il est VIP, la j'ai fait si Admin
				Main.queue_server.addPlayer((Player)sender, "factions", ((admin) ? 1 : 0));
				return false;
			}
			sender.sendMessage("/"+this.cmd+" join - Permet de rejoindre la queue !");
			sender.sendMessage("/"+this.cmd+" leave - Permet de quitter la queue !");
			if(admin && this.main_cmd){
				sender.sendMessage("/"+this.cmd+" pause - Permet de mettre la queue en pause !");
				sender.sendMessage("/"+this.cmd+" time <temps en seconde>  - Permet de changer le temps entre chaque personne !");
				sender.sendMessage("/"+this.cmd+" bypass <player> - Permet d'ajouter un joueur au d�but de la queue !");
				sender.sendMessage("/"+this.cmd+" last <player> - Permet d'ajouter un joueur � la queue !");
			}
			return false;
		}
		
		if(a[0].equalsIgnoreCase("join") && main_cmd){
			if(!isPlayer){
				sender.sendMessage("seul un joueur peut faire cela !");
				return false;
			}
			//look si il est VIP, la j'ai fait si Admin
			Main.queue_server.addPlayer((Player)sender, "factions", ((admin) ? 1 : 0));
			return false;
		}
		
		if(a[0].equalsIgnoreCase("leave")){
			if(!isPlayer){
				sender.sendMessage("seul un joueur peut faire cela !");
				return false;
			}
			Main.queue_server.removePlayer((Player)sender);
			return false;
		}
		
		if(admin){
			if(a[0].equalsIgnoreCase("pause")){
				Main.queue_server.stop();
				sender.sendMessage("La queue � �t� mise en pause !");
			}else if(a[0].equalsIgnoreCase("time")){
				if(a.length == 1){
					sender.sendMessage("/"+this.cmd+" time <temps en seconde>  - Permet de changer le temps entre chaque personne !");
				}else{
					try {
						Main.queue_server.setDelay(Integer.valueOf(a[1])*20);
						sender.sendMessage("Le d�lai est maintenant de "+Integer.valueOf(a[1])+"s !");
					}catch(Exception e){
						sender.sendMessage(a[1]+" n'est pas un nombre !");
					}
				}
			}else if(a[0].equalsIgnoreCase("bypass")){
				if(a.length == 1){
					sender.sendMessage("/"+this.cmd+" bypass <player> - Permet d'ajouter un joueur au d�but de la queue !");
				}else{
					Player player = Bukkit.getPlayer(a[1]);
					if(player == null && (!player.isOnline())){
						sender.sendMessage("Joueur "+a[1]+" introuvable !");
						return false;
					}
					Main.queue_server.addPlayer(player, "factions", -1);
					sender.sendMessage("Le joueur "+player.getName()+" est maintenant premier de la queue !");
				}
			}else if(a[0].equals("last")){
				if(a.length == 1){
					sender.sendMessage("/"+this.cmd+" last <player> - Permet d'ajouter un joueur � la queue !");
				}else{
					Player player = Bukkit.getPlayer(a[1]);
					if(player == null && (!player.isOnline())){
						sender.sendMessage("Joueur "+a[1]+" introuvable !");
						return false;
					}
					Main.queue_server.addPlayer(player, "factions", 0);
					sender.sendMessage("Le joueur "+player.getName()+" est maintenant premier de la queue !");
				}
			}
		}
		
		return false;
	}
	

}
