package be.jeffcheasey88.test.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import be.jeffcheasey88.test.Main;
import be.jeffcheasey88.test.utils.TopUtils.TopInfo;

public class Jump {
	
	public static boolean enable;

	public static void init(Main main, FileConfiguration config){
		enable = true;
		spawn = main.getConfigLocation("jump.spawn");
		
		start = main.getConfigLocation("jump.start").getBlock();
		end = main.getConfigLocation("jump.end").getBlock();
		
		checkpoints = new ArrayList<>();
		for(String path : main.getConfig().getConfigurationSection("jump.checkpoints").getKeys(false)){
			checkpoints.add(main.getConfigLocation("jump.checkpoints."+path).getBlock());
		}
		if(checkpoints.size() < 1) checkpoints.add(main.getConfigLocation("jump.checkpoints.points1").getBlock());
		
		blocks = new ArrayList<>();
		for(String path : main.getConfig().getConfigurationSection("jump.blocks").getKeys(false)){
			blocks.add(main.getConfigLocation("jump.blocks."+path));
		}
		if(blocks.size() < 1) blocks.add(main.getConfigLocation("jump.blocks.block1"));
		
		int size = Main.jump_top.getSize();
		top_armor = new ArmorStand[size];
		Location loc = main.getConfigLocation("jump.top.armor");
		ArrayList<TopInfo> top = Main.jump_top.get();
		show = ((String)main.getConfig(config, "jump.top.message", "&a%pos% &9%player% &c: &b%time%")).replace("&", "�");
		for(int i = 0; i < size; i++){
			Entity entity = Main.getEntity(loc);
			if(entity == null){
				ArmorStand armor = (ArmorStand)loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
				armor.setGravity(false);
				armor.setVisible(false);
				armor.setBasePlate(false);
				armor.setArms(false);
				armor.setCustomNameVisible(true);
				TopInfo info = top.get(i);
				armor.setCustomName(show.replace("%pos%", ""+(i+1)).replace("%player%", info.getName()).replace("%time%", ""+info.getValue()));
				top_armor[i] = armor;
			}else{
				TopInfo info = top.get(i);
				((ArmorStand)entity).setCustomName(show.replace("%pos%", ""+(i+1)).replace("%player%", info.getName()).replace("%time%", ""+info.getValue()));
				top_armor[i] = ((ArmorStand)entity);
			}
			loc = loc.add(0, -0.3, 0);
		}
	}
	
	public static void moveHolo(Location loc){
		Location target = loc.clone();
		int i = -1;
		ArrayList<TopInfo> top = Main.jump_top.get();
		for(ArmorStand armor : top_armor){
			i+=1;
			if(armor == null || armor.isDead()){
				ArmorStand create = (ArmorStand)loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
				create.setGravity(false);
				create.setVisible(false);
				create.setBasePlate(false);
				create.setArms(false);
				create.setCustomNameVisible(true);
				TopInfo info = top.get(i);
				armor.setCustomName(show.replace("%pos%", ""+(i+1)).replace("%player%", info.getName()).replace("%time%", ""+info.getValue()));
				top_armor[i] = armor;
				target = target.add(0, -0.3, 0);
				continue;
			}
			armor.teleport(target);
			target = target.add(0, -0.3, 0);
		}
	}
	
	public static void removeHolo(){
		for(ArmorStand armor : top_armor){
			if(armor == null) continue;
			armor.remove();
		}
	}
	
	private static ArmorStand[] top_armor;
	private static String show;
	
	private static Location spawn;
	private static Block start;
	private static Block end;
	private static List<Block> checkpoints;
	private static List<Location> blocks;
	
	private static List<Player> into = new ArrayList<>();
	
	private static HashMap<Player, BukkitTask> map = new HashMap<>();
	private static HashMap<Player, HashMap<String, Integer>> values = new HashMap<>();
	
	public static int value(Player player, String key){
		if(player == null || key == null) return 0;
		if(values.containsKey(player)){
			HashMap<String, Integer> kv = values.get(player);
			if(kv.containsKey(key)) return kv.get(key);
		}
		return 0;
	}
	
	public static void key(Player player, String key, int value){
		if(player == null || key == null) return;
		HashMap<String, Integer> kv = new HashMap<>();
		if(values.containsKey(player)) kv = values.get(player);
		kv.put(key, value);
		values.put(player, kv);
	}
	
	public static void key(Player player){
		if(values.containsKey(player)) values.remove(player);
	}
	
	public static void listen(Player player){
		unlisten(player);
		BukkitTask task = new BukkitRunnable(){
			@Override
			public void run(){
				if(!enable) return;
				if(Main.duelManager.isInDuel(player)) return;
				Block block = player.getLocation().getBlock();
				if(block.equals(start)) {
					unlisten(player);
					start(player);
				}
			}
		}.runTaskTimer(Main.getInstance(), 10, 10);
		map.put(player, task);
	}
	
	public static void unlisten(Player player){
		if(map.containsKey(player)){
			map.get(player).cancel();
			map.remove(player);
		}
		isInto(player, false);
	}
	
	public static void quit(Player player){
		listen(player);
		player.teleport(spawn);
	}
	
	public static void spawn(Player player){
		player.teleport(start.getLocation().clone().add(0, 0.4, 0));
		unlisten(player);
		start(player);
	}
	
	public static void start(Player player){
		player.sendMessage("�7Vous commencez le �e�lJump �7!");
		isInto(player, true);
		key(player);
		key(player, "a", 1);
		key(player, "b", -1);
		key(player, "c", 0);
		BukkitTask task = new BukkitRunnable(){
			@Override
			public void run(){
				int c = value(player, "c");
				key(player, "c", (c+1));
				int a = value(player, "a");
				int b = value(player, "b");
				Block block = player.getLocation().getBlock();
				if(block.equals(end)){
					ArrayList<TopInfo> list = Main.jump_top.input(new TopInfo(player.getName(), (c+1)));
					int i = 0;
					for(TopInfo info : list){
						ArmorStand armor = top_armor[i];
						if(armor == null || armor.isDead()) {
							i+=1;
							continue;
						}
						armor.setCustomName(show.replace("%pos%", ""+(i+1)).replace("%player%", info.getName()).replace("%time%", ""+info.getValue()));
						i+=1;
						if(info.getName().equals(player.getName())){
							player.sendMessage("GG tu es top "+i);
						}
					}
					unlisten(player);
					listen(player);
					player.sendMessage("�7Vous avez fini le Jump !");
					player.teleport(spawn);
					return;
				}
				for(int i = 0; i < checkpoints.size(); i++){
					Block check = checkpoints.get(i);
					if(block.equals(check)){
						if(b < i){
							key(player, "b", i);
							key(player, "a", (a+1));
							player.sendMessage("�b�lCheckPoint �7!");
						}
						return;
					}
				}
				Location pLoc = player.getLocation();
				for(Location loc : blocks){
					if(loc.distance(pLoc) < 8.0){
						return;
					}
				}
				
				
				//teleport checkpoint
				if(a > 1){
					Block checkpoint = checkpoints.get(b);
					player.teleport(checkpoint.getLocation());
					a--;
					player.sendMessage("�7Il vous reste �a�l"+a+" vie"+((a > 1) ? "s":"")+" �7!");
					key(player, "a", a);
					return;
				}
				
				player.sendMessage("�7Vous tes �c�ltomber �7!");
				player.teleport(spawn);
				
				//quit
				unlisten(player);
				listen(player);
			}
		}.runTaskTimer(Main.getInstance(), 20, 20);
		map.put(player, task);
	}
	
	public static synchronized boolean isInto(Player player){
		return into.contains(player);
	}
	
	public static synchronized void isInto(Player player, boolean is){
		if(is){
			isInto(player, false);
			into.add(player);
			for(Player players : Bukkit.getOnlinePlayers()){
				if(players.equals(player)) continue;
				player.hidePlayer(players);
			}
		}else{
			if(into.contains(player)) into.remove(player);
			for(Player players : Bukkit.getOnlinePlayers()){
				if(players.equals(player)) continue;
				player.showPlayer(players);
			}
		}
	}

}
