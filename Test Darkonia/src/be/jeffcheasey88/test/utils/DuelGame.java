package be.jeffcheasey88.test.utils;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import be.jeffcheasey88.test.Main;
import be.jeffcheasey88.test.utils.DuelManager.Arena;

public class DuelGame {
	
	private Arena arena;
	
	private Player p1;
	private Player p2;
	private Stats stats;
	
	private BukkitTask task;
	
	public DuelGame(Arena arena){
		stats = Stats.Waitting;
		this.arena = arena;
	}
	
	public void reset(){
		if(stats.equals(Stats.Finish)){
			p1 = null;
			p2 = null;
			if(task != null){
				task.cancel();
				task = null;
			}
			this.stats = Stats.Waitting;
		}
	}
	
	public boolean isEmpty(){
		return(p1 == null && p2 == null);
	}
	
	public Stats getStats(){
		return this.stats;
	}
	
	public Arena getArena(){
		return this.arena;
	}
	
	public boolean join(Player player){
		if(player == null) return false;
		if(stats.equals(Stats.Waitting)){
			if(p1 == null){
				p1 = player;
			}else{
				p2 = player;
			}
			checkStart();
			return true;
		}
		return false;
	}
	
	public void leave(Player player, boolean quit){
		if(player == null) return;
		boolean isP1 = (player.equals(p1));
		if(stats.equals(Stats.Game)){
			if(quit){
				if(isP1){
					p2.sendMessage("�7Vous avez gagner le duel !");
				}else{
					p1.sendMessage("�7Vous avez gagner le duel !");
				}
			}else{
				p1.sendMessage("�c"+((isP1) ? p2.getName() : p1.getName())+" �7gagne le duel !");
				p2.sendMessage("�c"+((isP1) ? p2.getName() : p1.getName())+" �7gagne le duel !");
			}
			finish();
			return;
		}
		if(stats.equals(Stats.Starting)){
			if(task != null){
				task.cancel();
				task = null;
			}
			if(isP1){
				p2.sendMessage("�c"+p1.getName()+" �7� quitter le duel !");
			}else{
				p1.sendMessage("�c"+p2.getName()+" �7� quitter le duel !");
			}
			stats = Stats.Waitting;
		}
		if(isP1){
			p1 = null;
		}else{
			p2 = null;
		}
		if(!quit){
			clear(player);
		}
	}
	
	public void kill(Player player){
		if(stats.equals(Stats.Game)) leave(player, false);
	}
	
	private int time;
	
	private void checkStart(){
		if(stats.equals(Stats.Waitting)){
			this.stats = Stats.Starting;
			
			tpArena(p1, arena.getSpawn1());
			tpArena(p2, arena.getSpawn2());
			
			time = 5;
			
			task = new BukkitRunnable(){
				@Override
				public void run(){
					if(time < 1){
						pvp();
						cancel();
						task = null;
						return;
					}
					p1.sendTitle("�9Duel", "�cPvP �7dans �c"+time+"�7s");
					p2.sendTitle("�9Duel", "�cPvP �7dans �c"+time+"�7s");
					time-=1;
				}
			}.runTaskTimer(Main.getInstance(), 20, 20);
		}
	}
	
	private void pvp(){
		if(stats.equals(Stats.Starting)){
			stats = Stats.Game;
			p1.setWalkSpeed(1f);
			p2.setWalkSpeed(1f);
		}
	}
	
	private void tpArena(Player player, Location spawn){
		player.teleport(spawn);
		player.setWalkSpeed(0f);
	}
	
	private void finish(){
		if(stats.equals(Stats.Game)){
			stats = Stats.Finish;
		}
	}
	
	private void clear(Player player){
		
		player.getInventory().clear();
		player.setWalkSpeed(1f);
		//tp lobby
	}
	
	
	public static enum Stats{
		
		Waitting, Starting, Game, Finish;
		
	}

}
