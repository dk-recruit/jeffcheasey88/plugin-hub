package be.jeffcheasey88.test.utils;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import be.jeffcheasey88.test.Main;
import be.jeffcheasey88.test.utils.DuelGame.Stats;

public class DuelManager {
	
	private HashMap<Player, DuelGame> duelPlayer;
	
	private DuelGame[] duels;
	
	public DuelManager(int count){
		this.duels = new DuelGame[count];
		for(int i = 0; i < count; i++){
			this.duels[i] = new DuelGame(new Arena().read(Main.getInstance().getConfig(), "duel.arena"+(i+1)));
		}
		
		this.duelPlayer = new HashMap<>();
	}
	
	public boolean isInDuel(Player player){
		if(player == null) return false;
		if(duelPlayer.containsKey(player)) return true;
		return false;
	}
	
	public DuelGame getDuel(Player player){
		if(player == null) return null;
		if(duelPlayer.containsKey(player)) return duelPlayer.get(player);
		return null;
	}
	
	public boolean join(Player player){
		if(isInDuel(player)) return false;
		
		for(int i = 0; i < this.duels.length; i++){
			DuelGame duel = this.duels[i];
			if(duel.join(player)) return true;
		}
		return false;
	}
	
	
	public void leave(Player player, boolean quit){
		if(isInDuel(player)){
			DuelGame duel = duelPlayer.get(player);
			duel.leave(player, quit);
			duelPlayer.remove(player);
		}
	}
	
	public void kill(Player player){
		if(isInDuel(player)){
			DuelGame duel = duelPlayer.get(player);
			duel.kill(player);
			duelPlayer.remove(player);
		}
	}
	
	public void win(Player player){
		if(isInDuel(player)) duelPlayer.remove(player);
	}
	
	public boolean addDuel(){
		int count = duels.length;
		DuelGame[] newList = new DuelGame[(count+1)];
		for(int i = 0; i < count; i++){
			newList[i] = duels[i];
		}
		newList[count] = new DuelGame(new Arena().read(Main.getInstance().getConfig(), "duel.arena"+(count+1)));
		this.duels = newList;
		return true;
	}
	
	public boolean removeDuel(){
		int count = duels.length;
		if(count == 1) return false;
		DuelGame[] newList = new DuelGame[(count-1)];
		int pos = -1;
		for(int i = 0; i < count; i++){
			DuelGame game = duels[i];
			if(game.getStats().equals(Stats.Finish) || game.isEmpty()){
				pos = i;
				break;
			}
		}
		if(pos < 0) return false;
		int j = 0;
		for(int i = 0; i < (count-1); i++){
			if(i == pos) continue;
			newList[i] = duels[j];
			j+=1;
		}
		this.duels = newList;
		return true;
	}
	
	
	public static class Arena{
		
		public static boolean isPathFree(String name){
			if(name == null) return false;
			return (!(Main.getInstance().getConfig().contains("duel."+name)));
		}
	
		private String name;
		private Location spawn1;
		private Location spawn2;
		
		public Arena(){}
		
		public Arena read(FileConfiguration config, String path){
			if(config == null || path == null) return this;
			this.name = config.getString(path+".name");
			
			this.spawn1 = Main.getInstance().getConfigLocation(path+".spawn1");
			this.spawn2 = Main.getInstance().getConfigLocation(path+".spawn2");
			
			return this;
		}
		
		public Arena save(FileConfiguration config, String path){
			if(config == null || path == null) return this;
			if(this.name != null){
				config.set(path+".name", this.name);
			}
			if(this.spawn1 != null)
				Main.getInstance().saveLocation(path+".spawn1", spawn1);
			
			if(this.spawn2 != null)
				Main.getInstance().saveLocation(path+".spawn2", spawn2);
			return this;
		}
		
		public void setName(String name){
			this.name = name;
		}
		
		public void setSpawn(Location loc, int spawn){
			if(spawn == 1){
				this.spawn1 = loc;
			}else if(spawn == 2){
				this.spawn2 = loc;
			}
		}

		public String getName(){
			return this.name;
		}
		
		public Location getSpawn1(){
			return this.spawn1;
		}
		
		public Location getSpawn2(){
			return this.spawn2;
		}
	}
}
