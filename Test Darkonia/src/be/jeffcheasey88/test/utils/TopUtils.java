package be.jeffcheasey88.test.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class TopUtils {
	
	private File file;
	private int size;
	private ArrayList<TopInfo> list;
	
	public TopUtils(File file, int size) throws Exception{
		if(file == null) throw new Exception("top file is null...");
		if(size < 1) throw new Exception("top size can't be under 1 !");
		this.file = file;
		this.size = size;
		if(file.exists()){
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while((line = reader.readLine()) != null){
				String[] split = line.split(":");
				list.add(new TopInfo(split[0], Integer.parseInt(split[1])));
			}
			reader.close();
			if(list.size() > size){
				int dif = (list.size()-size);
				for(int i = 0; i < dif; i++){
					list.remove(size);
				}
			}
		}
	}
	
	public int getSize(){
		return this.size;
	}
	
	public synchronized ArrayList<TopInfo> get(){
		return this.list;
	}
	
	public ArrayList<TopInfo> input(TopInfo info){
		if(info == null || info.getName() == null) return null;
		ArrayList<TopInfo> top = get();
		top.add(info);
		top = a(top, size, false);
		this.list.clear();
		this.list.addAll(top);
		return top;
	}
	
	public void save() throws Exception{
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		for(TopInfo info : this.list){
			writer.write(info.getName()+":"+info.getValue());
		}
		writer.flush();
		writer.close();
	}
	
	//je l'ai pris de mon serveur, je l'ai fait y a un petit temps, normal si les conditions sont pas des meilleurs mdr
	public static ArrayList<TopInfo> a(ArrayList<TopInfo> list, int max, boolean upper){
		ArrayList<TopInfo> a = new ArrayList<>();
		ArrayList<TopInfo> b = new ArrayList<>();
		if(list != null && !(list.isEmpty())){
			if(max > 0){
				b.addAll(list);
				for(int i = 0; i < max; i++){
					TopInfo ti = b(b, upper);
					if(ti != null){
						b.remove(ti);
						a.add(ti);
					}
				}	
			}
		}
		return a;
	}
	
	private static TopInfo b(ArrayList<TopInfo> list, boolean upper){
		if(list != null && !(list.isEmpty())){
			int i = (upper ? 0 : Integer.MAX_VALUE);
			TopInfo top = null;
			for(TopInfo ti : list){
				if(ti != null){
					if(upper){
						if(ti.getValue() > i){
							i = ti.getValue();
							top = ti;
						}
					}else{
						if(ti.getValue() < i){
							i = ti.getValue();
							top = ti;
						}
					}
				}
			}
			if(top != null){
				return top; 
			}
		}
		return null;
	}
	
	public static class TopInfo {
		
		private int i;
		private String name;
			
		public TopInfo(String name, int i){
			this.name = name;
			this.i = i;
		}
			
		public String getName(){
			return this.name;
		}
			
		public int getValue(){
			return this.i;
		}
		
		public void setValue(int value){
			this.i = value;
		}

	}

}
