package be.jeffcheasey88.test.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class Queue {
	
	private HashMap<Integer, List<Player>> waitters;
	private HashMap<Player, Integer> lockers;
	private HashMap<Player, Integer> positions;
	private HashMap<Player, String> datas;
	private List<Player> removes;
	private Executor exec;
	private BukkitTask task;
	private int delay;
	private int fields;
	private JavaPlugin plugin;
	private boolean lock;
	
	public Queue(int fields, JavaPlugin plugin, Executor exec){
		this.waitters = new HashMap<>();
		this.fields = fields;
		for(int i = 0; i < fields; i++){
			this.waitters.put(i, new ArrayList<>());
		}
		this.datas = new HashMap<>();
		this.lockers = new HashMap<>();
		this.positions = new HashMap<>();
		this.removes = new ArrayList<>();
		this.exec = exec;
		this.plugin = plugin;
	}

	public Queue setDelay(int delay){
		this.delay = delay;
		return this;
	}
	
	public int getPosition(Player player){
		if(player == null) return 0;
		if(positions.containsKey(player)) return positions.get(player);
		return 0;
	}
	
	public String getData(Player player){
		if(player == null) return null;
		if(datas.containsKey(player)) return datas.get(player);
		return null;
	}
	
	public int getMax(Player player){
		if(player == null) return 0;
		for(int i = 0; i < fields; i++){
			List<Player> players = waitters.get(i);
			if(players.contains(player)) return players.size();
		}
		return 0;
	}
	
	public boolean contains(Player player){
		if(player == null) return false;
		return datas.containsKey(player);
	}
	
	public boolean addPlayer(Player player, String data, int field){
		if(player == null) return false;
		if(datas.containsKey(player)) return false;
		this.datas.put(player, data);
		if(this.lock){
			this.lockers.put(player, field);
			return true;
		}
		if(field >= 0){
			List<Player> players = this.waitters.get(field);
			players.add(player);
			positions.put(player, players.size());
		}else{
			List<Player> players = this.waitters.get((-field)-1);
			players.add(0, player);
			positions.put(player, 1);
		}
		return true;
	}
	
	public boolean removePlayer(Player player){
		if(player == null) return false;
		if(datas.containsKey(player)){
			datas.remove(player);
			positions.remove(player);
			if(lockers.containsKey(player)){
				lockers.remove(player);
				return true;
			}
			if(lock){
				removes.add(player);
				return true;
			}
			for(int i = 0; i < fields; i++){
				List<Player> players = this.waitters.get(i);
				if(players.contains(player)){
					players.remove(player);
					return true;
				}
			}

		}
		return false;
	}
	
	public void start(){
		stop();
		task = new BukkitRunnable(){
			@Override
			public void run(){
				lock();
				for(int i = 0; i < fields; i++){
					List<Player> players = waitters.get(i);
					if(players.size() < 1) continue;
					Player player = players.get(0);
					players.remove(0);
					if(exec.execute(player, datas.get(player))){
						datas.remove(player);
						positions.remove(player);
					}else{
						players.add(1, player);
					}
					int j = 0;
					for(Player waitter : players){
						j+=1;
						positions.put(waitter, j);
					}
					break;
				}
				unlock();
			}
		}.runTaskTimer(plugin, 0, delay);
	}
	
	private void lock(){
		this.lock = true;
	}
	
	private void unlock(){
		this.lock = false;
		for(Entry<Player, Integer> entry : this.lockers.entrySet()){
			int field = entry.getValue();
			if(field >= 0){
				List<Player> players = this.waitters.get(field);
				players.add(entry.getKey());
			}else{
				List<Player> players = this.waitters.get((-field)-1);
				players.add(0, entry.getKey());
			}
		}
		this.lockers.clear();
		for(Player player : removes){
			for(int i = 0; i < fields; i++){
				List<Player> players = this.waitters.get(i);
				if(players.contains(player)){
					players.remove(player);
					break;
				}
			}
		}
	}
	
	public boolean isRunning(){
		return (task != null);
	}
	
	public void stop(){ 
		if(task == null) return;
		task.cancel();
		task = null;
		unlock();
	}
	

	public static interface Executor{
		
		boolean execute(Player player, String data);
		
	}
}
