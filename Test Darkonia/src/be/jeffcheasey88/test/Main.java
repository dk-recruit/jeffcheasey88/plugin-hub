package be.jeffcheasey88.test;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import be.jeffcheasey88.test.command.DuelCommand;
import be.jeffcheasey88.test.command.JumpCommand;
import be.jeffcheasey88.test.command.QueueCommand;
import be.jeffcheasey88.test.utils.DuelGame;
import be.jeffcheasey88.test.utils.DuelGame.Stats;
import be.jeffcheasey88.test.utils.DuelManager;
import be.jeffcheasey88.test.utils.Jump;
import be.jeffcheasey88.test.utils.Queue;
import be.jeffcheasey88.test.utils.TopUtils;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class Main extends JavaPlugin implements Listener{
	
	//rapel ceci est un plugin de test, il manque bien �videment certain syst�me tel que pour tp le joueur sur les serveurs,
	//verifier son grade,...
	
	
	//voila il ne reste plus que les checkpoint du jump ansi que les blocks en faisant parti =D
	//Comme c'est un plugin de test je m'arr�te la pour l'instant.
	
	private static Main pl;
	
	public static Main getInstance(){
		return pl;
	}
	
	private static boolean duel;
	private static boolean jump;
	private static int delay_server;
	private static int delay_duel;
	
	private static String actionBar_server;
	private static String actionBar_server_jump;
	private static String actionBar_server_duel;
	private static String actionBar_duel;
	
	public static Queue queue_server;
	public static Queue queue_duel;
	
	public static DuelManager duelManager;
	public static TopUtils jump_top;
	
	public void onEnable(){
		pl = this;
		
		initConfig();
		
		queue_server = new Queue(2, this, new Queue.Executor(){
			@Override
			public boolean execute(Player player, String data){
				if(player == null || data == null) return true;
				if(duelManager.isInDuel(player)) return false;
				if(Jump.isInto(player)) return false;
				if(queue_duel.contains(player)) queue_duel.removePlayer(player);
				//send player sur le server qui s'appel data
				return true;
			}
		}).setDelay(delay_server);
		
		queue_duel = new Queue(1, this, new Queue.Executor(){
			@Override
			public boolean execute(Player player, String data){
				if(player == null) return true;
				if(Jump.isInto(player)) return false;
				return duelManager.join(player);
			}
		}).setDelay(delay_duel);
		
		new QueueCommand(this, "queue", true);
		new QueueCommand(this, "factions", false);
		
		getCommand("duel").setExecutor(new DuelCommand());
		if(jump) getCommand("jump").setExecutor(new JumpCommand());
		
		queue_server.start();
		if(duel) queue_duel.start();
		
		new BukkitRunnable(){
			@Override
			public void run(){
				for(Player player : Bukkit.getOnlinePlayers()){
					String server = null;
					String duel = null;
					if(queue_server.contains(player)){
						int pos = queue_server.getPosition(player);
						int max = queue_server.getMax(player);
						String servername = queue_server.getData(player);
						if(duelManager.isInDuel(player)){
							server = actionBar_server_duel.replace("%server%", servername).replace("%position%", ""+pos).replace("%max%", ""+max);
						}else if(Jump.isInto(player)){
							server = actionBar_server_jump.replace("%server%", servername).replace("%position%", ""+pos).replace("%max%", ""+max);
						}else{
							server = actionBar_server.replace("%server%", servername).replace("%position%", ""+pos).replace("%max%", ""+max);
						}
					}
					if(queue_duel.contains(player)){
						int pos = queue_duel.getPosition(player);
						int max = queue_duel.getMax(player);
						duel = actionBar_duel.replace("%position%", ""+pos).replace("%max%", ""+max);
					}
					if(server != null && duel != null){
						sendActionBar(player, server+" &7| "+duel);
					}else if(server == null && duel == null) continue;
					sendActionBar(player, ((server != null) ? server : duel));
				}
			}
		}.runTaskTimer(this, 30, 30);
		
		Bukkit.getPluginManager().registerEvents(this, this);
		
		if(jump){
			try {
				jump_top = new TopUtils(new File((String)getConfig(getConfig(), "jump.top.path", "/home/jump.top")), (int)getConfig(getConfig(), "jump.top.size", 10));
				Jump.init(this, getConfig());
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void sendActionBar(Player player, String message){
		IChatBaseComponent json = ChatSerializer.a("{\"text\": \""+message+ "\"}");
		PacketPlayOutChat packet = new PacketPlayOutChat(json, (byte) 2);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
	
	private void initConfig(){
		FileConfiguration config = getConfig();
		
		delay_server = ((int) getConfig(config, "server.delay", 5))*20;
		
		duel = (boolean) getConfig(config, "duel.enable", true);
		if(duel){
			delay_duel = ((int) getConfig(config, "duel.delay", 5))*20;
			duelManager = new DuelManager((int)getConfig(config, "duel.count", 1));
		}
		
		jump = (boolean) getConfig(config, "jump.enable", true);
		
		actionBar_server = ((String) getConfig(config, "server.actionbar", "&7(�9%server%&7) Vous �tes &c%position%&7/&c%max%")).replace("&", "�");
		actionBar_server_jump = ((String) getConfig(config, "server.actionbar_jump", actionBar_server.replace("�", "&")+" &7- Attente &eJump")).replace("&", "�");
		actionBar_server_duel = ((String) getConfig(config, "server.actionbar_duel", actionBar_server.replace("�", "&")+" &7- Attente &cDuel")).replace("&", "�");
		actionBar_duel = ((String) getConfig(config, "duel.actionbar", "&7(&cDuel&7) Vous �tes &c%position%&7/&c%max%")).replace("&", "�");
	}
	
	public Object getConfig(FileConfiguration config, String path, Object default_value){
		if(config == null) return null;
		if(config.get(path) == null){
			config.set(path, default_value);
			saveConfig();
			return default_value;
		}
		return config.get(path);
	}
	
	public Location getConfigLocation(String path){
		FileConfiguration config = getConfig();
		return new Location(Bukkit.getWorld((String)getConfig(config, path+".world", Bukkit.getWorlds().get(0).getName())),
				(double)getConfig(config, path+".x", 0.0), (double)getConfig(config, path+".y", 0.0), (double)getConfig(config, path+".z", 0.0),
				(float)getConfig(config, path+".yaw", 0f), (float)getConfig(config, path+".pitch", 0f));
	}
	
	public void saveLocation(String path, Location location){
		FileConfiguration config = getConfig();
		config.set(path+".world", location.getWorld().getName());
		config.set(path+".x", location.getX());
		config.set(path+".y", location.getY());
		config.set(path+".z", location.getZ());
		config.set(path+".yaw", location.getYaw());
		config.set(path+".pitch", location.getPitch());
		saveConfig();
	}

	@EventHandler
	public void onDamage(EntityDamageEvent event){
		Entity entity = event.getEntity();
		if(entity instanceof Player){
			event.setCancelled(true);
			Player player = (Player)entity;
			DuelGame duel = duelManager.getDuel(player);
			if(duel == null) return;
			if(duel.getStats().equals(Stats.Game)) event.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		for(Player players : Bukkit.getOnlinePlayers()){
			if(players.equals(player)) return;
			if(Jump.isInto(players)) players.hidePlayer(player);
		}
		Jump.listen(player);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event){
		Jump.unlisten(event.getPlayer());
		duelManager.kill(event.getPlayer());
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent event){
		duelManager.kill(event.getEntity());
	}
	
	public static Entity getEntity(Main main, String path){
		return getEntity(main.getConfigLocation(path));
	}
	
	public static Entity getEntity(Location loc){
		if(loc == null) return null;
		World world = loc.getWorld();
		Entity best = null;
		double distance = Double.MAX_VALUE;
		for(Entity entity : world.getEntities()){
			double dis = entity.getLocation().distance(loc);
			if(dis == 0.0) return entity;
			if(dis < distance){
				best = entity;
				distance = dis;
			}
		}
		return best;
	}
}
